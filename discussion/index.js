// DIFFERENCE

	/*
		JS Objects -only has the value inserted inside the quotation marks
		JSON has the quotation marks for both key and th values


		JS Objects - exclusive for javascript
		JSOn - not exclusive for javascript. other programming languages can also use JSON files
	*/


//JSON Object
/*
	JavasScript Object Notation
	JSON - used for serializing/deserializing different data types into bytes
		serialization - process of converting data into series of bytes for easier  transmission or transfer of information
*/
/*let address = {
	"city": "Cagayan de Oro City",
	"province": "Misamis Oriental",
	"country": "Philippines"
};
console.log(address);*/


//JSON Arrays
/*let cities = [
{
	"city": "Cagayan de Oro City",
	"province": "Misamis Oriental",
	"country": "Philippines"
},

{
	"city": "Ormoc",
	"province": "Leyte",
	"country": "Philippines"
},

{
	"city": "Malaybalay",
	"province": "Bukidnon",
	"country": "Philippines"
}
];

console.log(cities);*/

let batches = [
{
	"batchName": "Batch X"
},
{
	"batchName": "Batch Y"
}
];
console.log("Result from console.log");
console.log(batches);

console.log("Result from stringify method");
console.log(JSON.stringify(batches));


let data = JSON.stringify({
	name: "John",
	age: 31,
	address:{
		city: "Manila",
		country: "Philippines"
	}
});
console.log(data);


/*let userDetails = JSON.stringify(
{
	fname:prompt('Please input your first name.'),
	lname:prompt('Please input your last name.'),
	age:prompt('Please input your age.'),
	Address:{ 
		city:prompt('City?'),
		country:prompt('Country?'),
		zipCode:prompt('Zip Code?')
	}
});
console.log(userDetails);*/


//CONVERTING OF STRINGIFIED JSON INTO JS OBJECTS
	//PARSE Method- converting json data into js objects
	//information is commonly sent to application in STRINGIFIED JSON; then converted into objects; this happens both for sending information to a backend app such as database and back to front-end app such as the webpages
	//upon receiving the data, JSON text can be converted into a JS/JSON Object with parse method
let batchesJSON = `[
{
	"batchName": "Batch X"
},
{
	"batchName": "Batch Y"
}
]`;
console.log(batchesJSON);

console.log("Result from parse method:");
console.log(JSON.parse(batchesJSON));


let stringifiedData = `{
	"name": "John",
	"age": "31",
	"address":{
		"city": "Manila",
		"country": "Philippines"
	}
}`;

console.log(JSON.parse(stringifiedData));
